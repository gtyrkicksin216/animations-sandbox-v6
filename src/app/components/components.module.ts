import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NavLinksModule } from '@components/nav-links/nav-links.module';
import { ShrinkModule } from '@components/shrink/shrink.module';
import { SliderModule } from '@components/slider/slider.module';
import { SpinnerModule } from '@components/spinner/spinner.module';
import { ChildStaggerModule } from '@components/child-stagger/child-stagger.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    NavLinksModule,
    ShrinkModule,
    SliderModule,
    SpinnerModule,
    ChildStaggerModule,
  ],
  declarations: [ ],
  exports: [
    NavLinksModule,
    ShrinkModule,
    SliderModule,
    SpinnerModule,
    ChildStaggerModule,
  ],
})

export class ComponentsModule { }
