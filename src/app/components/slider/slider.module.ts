import { NgModule } from '@angular/core';
import { SliderComponent } from '@components/slider/slider.component';

@NgModule({
  imports: [  ],
  declarations: [ SliderComponent ],
  exports: [ SliderComponent ],
})

export class SliderModule { }
