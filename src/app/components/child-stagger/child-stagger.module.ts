import { NgModule } from '@angular/core';
import { ChildStaggerComponent } from '@components/child-stagger/child-stagger.component';

@NgModule({
  imports: [  ],
  declarations: [ ChildStaggerComponent ],
  exports: [ ChildStaggerComponent ],
})

export class ChildStaggerModule { }
