import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavLinksComponent } from '@components/nav-links/nav-links.component';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ NavLinksComponent ],
  exports: [ NavLinksComponent ],
})

export class NavLinksModule { }
