import { NgModule } from '@angular/core';
import { ShrinkComponent } from '@components/shrink/shrink.component';

@NgModule({
  imports: [  ],
  declarations: [ ShrinkComponent ],
  exports: [ ShrinkComponent ],
})

export class ShrinkModule { }
